### Description

* Create an image that can be used inside a Kubernetes clusters to deploy Kubernetes objects inside that same cluster
* The image contains `kubectl`, `envsubst` and run with the `kube-deployer` user
* Have a look at the project [container registry](https://gitlab.com/idrsk/gke-deployer/container_registry/2744364) for the container images
* After the `1.0.1` tag, images tags corresponds to the version of `kubectl` used inside the image

### Usage example of envsubst

`envsubst` can be used to inject data inside Kubernetes manifest files during deployment. That can be useful for instance to inject secrets whithout revealing their values inside a Git repository or dynamically inject app version data into the manifest. The injected data values are taken from the deployment system environment variables. 

Here is the content of the Kubernetes manifest file used to deploy `myapp`.

```
# deploy.yml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp
  labels:
    app: myapp
spec:
  replicas: 1
  selector:
    matchLabels:
      app: myapp
  template:
    metadata:
      labels:
        app: myapp
    spec:
      imagePullSecrets:
        - name: myapp
      containers:
      - name: myapp
        image: registry.example.com/superapps/myapp:1.2.1
        ports:
          - name: http
            containerPort: 8080
        env:
          - name: ACCESS_TOKEN
            value: "$ACCESS_TOKEN"
        resources:
          limits:
            memory: "500Mi"
          requests:
            memory: "500Mi"
```

That manifest is present inside a Git repository so we don't want to put secrets inside. Instead we put secret variables values as shell variables inside the manifest file : `$ACCESS_TOKEN` in this example. The `$ACCESS_TOKEN` real value is present in our deployment system environment variables. To update that value on the fly during deployment, we use `envsubst` like this :

```
cat deploy.yml | envsubst | kubectl apply -f -
```

Passing the `deploy.yml` file content to `envsubst` will replace any `$VAR` inside that file by the system `VAR` environment
variable value and output the result. In our case, if an environment variable named `ACCESS_TOKEN` is present inside the deployment system environment variables, its value will be taken to replace `$ACCESS_TOKEN` inside the `deploy.yml` kubernetes manifest file and the resulting content will be printed to stdout. Then we simply send that result to the input of the `kubectl apply -f` command to deploy the manifest object with the secrets it needs, without revealing them inside the Git repository.

### Build the container image

```
# Syntax
docker build -t <name>:<tag>
```

* `name` : the image name. Must contains the container registry URL if the image will be pushed into a container registry
* `tag` : image tag. Example: 1.0.1

```
# Examples

# Build for local use
docker build gke-deployer:1.0.1

# Build for container registry
docker build registry.gitlab.com/idrsk/gke-deployer:1.0.1
```

### Push to container registry

* Authentication with the container registry

```
# Syntax
docker login <registry-url>

# Example
docker login registry.gitlab.com
```

* Push image into the container registry

```
# Syntax
docker push <registry-url>/<namespace>/<image-name>:<image-tag>

# Example
docker push registry.gitlab.com/idrsk/gke-deployer:1.0.1
```
